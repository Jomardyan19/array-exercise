﻿using System;

namespace ConsoleApp79
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 45, 96, 74, 25, 12, 36, 96 };
            int max = arr[0];
            int min = arr[0];
            int minIndex = 0;
            int maxIndex = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (max < arr[i])
                {
                    max = arr[i];
                    maxIndex = i;
                }
                if (min > arr[i])
                {
                    min = arr[i];
                    minIndex = i;
                }
            }

            Console.WriteLine($"Minimal value is {min},maximal value is {max}");
            arr[minIndex] = max;
            arr[maxIndex] = min;
            Console.WriteLine($"The change implying {min} to {max}");

            //For test
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }

            Console.ReadLine();

        }
    }
}
